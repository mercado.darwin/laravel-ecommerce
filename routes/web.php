<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//Catalog
Route::get('/catalog', 'ItemController@index');

Route::get('/additem', 'ItemController@create');

Route::post('/additem', 'ItemController@store');

Route::delete('/deleteitem/{id}', 'ItemController@destroy');

Route::get('/edititem/{id}', 'ItemController@update');

Route::post('/addtocart/{id}', 'ItemController@addtocart');

Route::get('/cart', 'ItemController@showCart');

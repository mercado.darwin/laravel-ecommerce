@extends('layouts.app')
@section('content')
	<h1 class="text-center py-5">Edit Item Form</h1>
	<div class="col-lg-4 offset-lg-4 bg-info">
		<form action="/additem" method="POST" enctype="multipart/form-data">
			@csrf
			@method('PATCH')
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group">
				<label for="description">Description:</label>
				<input type="text" name="body" class="form-control">
			</div>
			<div class="form-group">
				<label for="price">Price: </label>
				<input type="text" name="price" class="form-control">
			</div>
			<div class="form-group">
				<label for="imgPath">Image:</label>
				<input type="file" name="imgPath" class="form-control">
			</div>
			<div class="form-group">
				<label for="category_id">Category: </label>
				<select name="category_id" class="form-control">
					@foreach($categories as $indiv_category)
						<option value="{{$indiv_category->id}}">{{$indiv_category->name}}</option>
					@endforeach
				</select>
			</div>
			<button type="submit" class="btn btn-success">Edit</button>
		</form>
	</div>
	
@endsection
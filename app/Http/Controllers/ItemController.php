<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Item;
use \App\Category;
use \App\Order;
use Session;

class ItemController extends Controller
{
    public function index(){
    	$items = Item::all();

    	return view('/catalog', compact('items'));
    }

    public function create(){
    	$categories = Category::all();

    	return view('adminviews.additem', compact('categories'));
    }

    public function store(Request $req){
    	// validate
    	$rules = array(
    		"name" => "required",
    		"description" => "required",
    		"price" => "required|numeric",
    		"category_id" => "required",
    		"imgPath" => "required|image|mimes:jpeg,jpg,png,gif,tiff,tif,webp,bitmap"
    	);

    	$this->validate($req, $rules);

    	// dd($req);
        $newItem = new Item;
        $newItem->name = $req->name;
        $newItem->description = $req->description;
        $newItem->price = $req->price;
        $newItem->category_id = $req->category_id;

    	// capture
        $image = $req->file('imgPath');
        $image_name = time().".".$image->getClientOriginalExtension();
        $destination = "image/"; // corresponds to images directory
        $image->move($destination, $image_name);
        $newItem->save();
        Session::flash('message', "$newItem->name: has been added");
        return redirect('/catalog');
    	// save
    	// redirect
    }

    public function destroy($id){
        $itemToDelete = Item::find($id);
        $itemToDelete->delete();

        Session::flash('message',"$itemToDelete->name has been deleted");
        return redirect()->back();
    }

    public function edit(){

    }


    public function update($id, Request $req){
        $item = Item::find($id);

        $rules = array(
            "name" => "required",
            "description" => "required",
            "price" => "required|numeric",
            "category_id" => "required",
            "imgPath" => "required|image|mimes:jpeg,jpg,png,gif,tiff,tif,webp,bitmap"            
        );
            $this->validate($req, $rules);

            // dd($req);
            $item = new Item;
            $item->name = $req->name;
            $item->description = $req->description;
            $item->price = $req->price;
            $item->category_id = $req->category_id;

            if($req->file('imgPath') != null){
                $image = $req->file('imgPath');
                $image_name = time().".".$image->getClientOriginalExtension();
                $destination = "image/"; // corresponds to images directory
                $image->move($destination, $image_name);
                $item->imgPath = $destination.$image_name;
                
            }
            $item->save();
            Session::flash('message', "$Item->name: has been updated");
            return redirect('/catalog');
        
    }

    public function addtocart($id, Request $req){
        // check if there is an existing session
        
        if (Session::has('cart')) {
            $cart = Session::get('cart');
        }else{
            $cart = [];
        }

        
        // check is this is the first time we'll add an item to our cart
        if (isset($cart[$id])) {
            $cart[$id] = $req->quantity;
        }else{
            $cart[$id] = $req->quantity;
        }


        $item = Item::find($id);

        Session::put("cart", $cart);
        Session::flash('message', "$req->quantity of $item->name: successfully to cart");
        return redirect()->back();
    }

    public function showCart(){
        // we will create a new array containing item_name, price, quantity and subtotal
        // initialize an empty array
        $items = [];
        $total = 0;
        if (Session::has('cart')) {
            $cart = Session::get('cart');
            foreach ($cart as $itemId => $quantity) {
                $item = Item::find($itemId);
                $item->quantity = $quantity;
                $item->subtotal = $item->price * $quantity;
                $items[] = $item;
                $total += $item->subtotal;
            }
        }


        return view("userviews.cart", compact('items', 'total'));
    }
}
